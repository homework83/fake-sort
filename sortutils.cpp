#include "sortutils.h"

#include <fstream>
#include <iostream>

namespace po = boost::program_options;

std::tuple<po::options_description, po::variables_map> configureOption(int argc, char** argv)
{
    po::options_description desc("Name: fake-sort\nDescription: sort a set of strings.\nUsage: ./fake-sort [options] string1 string2 string3 ...\nExample:./fake-sort -c dd ff aa nn bb gg bb ftdr aa bb dd ff ftdr gg nn \nInputs");
    desc.add_options()("help,h", "print usage message");
    desc.add_options()("invert-sort,v", "sort in inverse order");
    desc.add_options()("clean,c", "remove redundant words");
    desc.add_options()("input-words", po::value<std::vector<std::string>>(), "input words");

    // Parse command line arguments
    po::positional_options_description p;
    p.add("input-words", -1);

    po::variables_map vm;
    po::store(po::command_line_parser(argc, argv).options(desc).positional(p).run(), vm);
    po::notify(vm);
    return { desc, vm };
}

std::tuple<bool, bool, bool> parseCommandLine(po::variables_map const vm)
{
    bool invertSort = false;
    bool cleanDuplicate = false;

    if (vm.count("help")) {
        return { false, invertSort, cleanDuplicate };
    }

    if (!vm.count("input-words")) {
        std::cerr << "Missing input words "
                  << "\n";
        return { false, invertSort, cleanDuplicate };
    }
    if (vm.count("invert-sort")) {

        invertSort = true;
    }
    if (vm.count("clean")) {

        cleanDuplicate = true;
    }
    return { true, invertSort, cleanDuplicate };
}

std::vector<std::string> sortVect(std::vector<std::string> inputWords, bool const invertSort, bool const cleanDuplicate)
{
    std::sort(inputWords.begin(), inputWords.end());

    if (cleanDuplicate) {
        std::vector<std::string>::iterator it;
        it = std::unique(inputWords.begin(), inputWords.end());
        // remove empty positions
        inputWords.resize(std::distance(inputWords.begin(), it));
    }

    if (invertSort) {
        std::reverse(inputWords.begin(), inputWords.end());
    }
    return inputWords;
}

void printVect(std::vector<std::string> const inputWords)
{
    for (std::string word : inputWords) {
        std::cout << word << "\t";
    }
    std::cout << "\n";
}