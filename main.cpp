#include "sortutils.h"
#include <fstream>
#include <iostream>
#include <program_options.hpp>
#include <tuple>

int main(int argc, char** argv)
{

    auto [desc, vm] = configureOption(argc, argv);

    auto [result, invert, clean] = parseCommandLine(vm);

    if (!result) {
        std::cerr << desc << "\n";
        return 1;
    }

    std::vector<std::string> inputWords = vm["input-words"].as<std::vector<std::string>>();

    inputWords = sortVect(inputWords, invert, clean);
    printVect(inputWords);
}
