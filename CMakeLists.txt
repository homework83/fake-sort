cmake_minimum_required(VERSION 3.5)

project(fake-sort LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

find_package(Boost 1.67.0 COMPONENTS program_options REQUIRED)
set(Boost_INCLUDE_DIR  /usr/include/boost/)
set(Boost_LIBRARY_DIR  /usr/lib/x86_64-linux-gnu/)
include_directories(${Boost_INCLUDE_DIR})
link_directories(${Boost_LIBRARY_DIR})

set(SRCS
    main.cpp
    sortutils.cpp
    )
    
set(HEADERS
    sortutils.h
    )
add_executable(fake-sort ${SRCS} ${HEADERS})
target_link_libraries(fake-sort Boost::program_options)

add_subdirectory(unit-test)
