fake-sort
=========
Description
============
This programm allows to sort a list of strings. Strings are entered on the command line

Allowed options:


 * `-h [ --help ]` print usage message

 * `-v [ --invert-sort ]` sort in inverse order

 * `-c [ --clean ]` remove redundant words

 * `--input-words` arg input words

Examples 
========

```
./fake-sort -c dd ff aa nn bb gg bb ftdr aa bb dd ff ftdr gg nn

./fake-sort -cv dd ff aa nn bb gg bb ftdr nn gg ftdr ff dd bb aa

./fake-sort dd ff aa nn bb gg bb ftdr aa bb bb dd ff ftdr gg nn
```

Prerequisite
============

Following tools should be installed before tool compilation (Ubuntu 21.04): 

1. g++ compilation tools 

```
sudo apt-get install build-essential
```

2. cmake 

```
sudo apt-get install cmake
```

3. Boost

```
sudo apt-get install libboost-all-dev
```

4. git

```
sudo apt install git
```

5. gtest 

```
sudo apt-get install libgtest-dev
cd /usr/src/gtest
sudo cmake CMakeLists.txt
sudo make
sudo cp lib/*.a /usr/lib/
```

Tool installation
=================
1. First you should download source code from gitlab by executing:

```
git clone https://gitlab.com/homework83/fake-sort.git fake-sort
```

2. Then you should compile the tool:

```
cd fake-sort

cmake .

make

```

`fake-sort` command is now ready to be used. 

Unit test execution 
===================

To run unit test execute following commands:
```
cd fake-sort/unit-test/
./unit-test 
[==========] Running 1 test from 1 test suite.
[----------] Global test environment set-up.
[----------] 1 test from sortutilsTest
[ RUN      ] sortutilsTest.sortVect
[       OK ] sortutilsTest.sortVect (0 ms)
[----------] 1 test from sortutilsTest (0 ms total)

[----------] Global test environment tear-down
[==========] 1 test from 1 test suite ran. (0 ms total)
[  PASSED  ] 1 test.

```
