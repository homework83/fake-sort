#include "../sortutils.h"
#include "gtest/gtest.h"

TEST(sortutilsTest, sortVect)
{

    std::vector<std::string> inputWords { "aa", "vv", "ggg", "cc", "aa", "ff" };

    // case 1: invert sorting  and remove duplicates  options are disabled
    EXPECT_EQ(std::vector<std::string>(sortVect(inputWords, false, false)), std::vector<std::string>({ "aa", "aa", "cc", "ff", "ggg", "vv" }));

    // case 2: invert sorting is disbaled  and remove duplicates is enabled
    EXPECT_EQ(std::vector<std::string>(sortVect(inputWords, false, true)), std::vector<std::string>({ "aa", "cc", "ff", "ggg", "vv" }));

    // case 3: invert sorting is enabled and remove duplicates is disabled
    EXPECT_EQ(std::vector<std::string>(sortVect(inputWords, true, false)), std::vector<std::string>({ "vv", "ggg", "ff", "cc", "aa", "aa" }));

    // case 4: invert sorting and remove duplicates options are enabled
    EXPECT_EQ(std::vector<std::string>(sortVect(inputWords, true, true)), std::vector<std::string>({ "vv", "ggg", "ff", "cc", "aa" }));
}

int main(int argc, char** argv)
{
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
