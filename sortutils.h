#ifndef SORTUTILS_H
#define SORTUTILS_H

#include <program_options.hpp>
#include <tuple>

namespace po = boost::program_options;

std::tuple<po::options_description, po::variables_map> configureOption(int argc, char** argv);
std::tuple<bool, bool, bool> parseCommandLine(po::variables_map vm);
std::vector<std::string> sortVect(std::vector<std::string> inputWords, bool v, bool c);
void printVect(std::vector<std::string> inputWords);

#endif // SORTUTILS_H
